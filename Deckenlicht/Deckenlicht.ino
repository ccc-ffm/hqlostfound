
#define ledRed 9
#define ledGreen 10
#define ledBlue 11

#define RED 0
#define GREEN 1
#define BLUE 2

#define cnt_0_to_127 ((counter%256)/2)
#define cnt_127_to_0 (127 - cnt_0_to_127)
#define cnt_0_to_63  (cnt_0_to_127/2)
#define cnt_63_to_0  (63 - cnt_0_to_63)


byte rgb[] = { 0, 80, 160 };
byte nextStaticRGB[] = { 0, 0, 0 };

byte staticAvailable = 0;
byte serialRGBPos = RED;
unsigned long lastSerialDataTS = 0;

short counter = 0;

void setup()  { 
  Serial.begin(9600);
}

void loop()  {
    if (Serial.available() > 0) {
      if (lastSerialDataTS + 1000 < millis()) {
        serialRGBPos = RED;
      }
      lastSerialDataTS = millis();
      nextStaticRGB[serialRGBPos++] = Serial.read();
      if ( serialRGBPos > BLUE) {
        rgb[RED] = nextStaticRGB[RED];
        rgb[GREEN] = nextStaticRGB[GREEN];
        rgb[BLUE] = nextStaticRGB[BLUE];
        serialRGBPos = RED;
      }
      staticAvailable = nextStaticRGB[RED] | nextStaticRGB[GREEN] | nextStaticRGB[BLUE];
    }

    if (staticAvailable == 0) {
      if (counter < 256){ // blue -> violett
        rgb[RED] = cnt_0_to_63;
        rgb[GREEN] = 0;
        rgb[BLUE] = 128 + cnt_127_to_0;
      }else if (counter < 2*256) { // violett -> red
        rgb[RED] = 64 + cnt_0_to_63;
        rgb[GREEN] = 0;
        rgb[BLUE] = cnt_127_to_0;
      }else if (counter < 3*256){ // red -> yellow
        rgb[RED] = 64 + cnt_63_to_0;
        rgb[GREEN] = cnt_0_to_63;
        rgb[BLUE] = 0;
      }else if (counter < 4*256){ // yellow -> green
        rgb[RED] = cnt_63_to_0;
        rgb[GREEN] = 64 + cnt_0_to_63;
        rgb[BLUE] = 0;
      }else if (counter < 5*256){ // green -> turquoise
        rgb[RED] = 0;
        rgb[GREEN] = 64 + cnt_63_to_0;
        rgb[BLUE] = cnt_0_to_127;
      }else if (counter < 6*256){ // turquoise -> blue
        rgb[RED] = 0;
        rgb[GREEN] = cnt_63_to_0;
        rgb[BLUE] = 128 + cnt_0_to_127;
      }
      else counter = 0;
      counter += 1;
    }
    analogWrite(ledRed, rgb[RED]); 
    analogWrite(ledGreen, rgb[GREEN]); 
    analogWrite(ledBlue, rgb[BLUE]); 
    delay(15);
}

