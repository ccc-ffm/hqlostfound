#!/bin/bash

## cat > /etc/udev/rules.d/99-ledstripe.rules <'EOF#
#
# KERNEL=="tty*", SUBSYSTEM=="tty", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0001", ATTRS{serial}=="6493234373835111F0B1", GROUP="www-data", MODE="0660", SYMLINK+="ledstripe", RUN+="/bin/stty -F /dev/ledstripe 9600 -hupcl"
#

cat <<'EOF'
Content-Type: text/html

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>LEDSTRIP COLOR</title>
</head>
<body>
EOF

color='#000000'
# http://192.168.2.12/cgi-bin/ledcolor.sh?color=%23ff00ff
case "${QUERY_STRING}" in
  color=%23[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F])
    color="#${QUERY_STRING:9}"
    echo -en "\x${QUERY_STRING:9:2}\x${QUERY_STRING:11:2}\x${QUERY_STRING:13:2}" > /dev/ledstripe
  ;;
esac

cat <<EOF
<form method="GET"><input type="color" name="color" value="${color}"><input type="submit"></form>
</form>
</body>
</html>
EOF

