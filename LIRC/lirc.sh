#!/bin/bash

LIRC_IP="192.168.2.12"
LIRC_PORT="8765"

cat <<'EOF'
Content-Type: text/html

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>LIRC</title>
</head>
<body>
EOF

# set -x; exec 2>&1

# http://192.168.2.12/cgi-bin/lirc.sh
cmd="LIST"

if [ -n "${PATH_INFO#/}" ]
then
  if [ -z "${QUERY_STRING}" ]
  then
    # http://192.168.2.12/cgi-bin/lirc.sh/MOODLAMP
    cmd="LIST ${PATH_INFO#/}"
  else
    # http://192.168.2.12/cgi-bin/lirc.sh/MOODLAMP?PAUSE
    cmd="SEND_ONCE ${PATH_INFO#/} ${QUERY_STRING}"
  fi
fi

exec 10<> /dev/tcp/${LIRC_IP}/${LIRC_PORT}

echo "${cmd}" >&10

data_n=-1
wait_for_n=0
relistcmds=0
while read -u 10 -a response 
do

  # echo '<li>'"${response[@]}"'</li>'

  if [ $data_n -gt 0 ]
  then

    link="${SCRIPT_NAME}"

    if [ -z "${PATH_INFO#/}" ]
    then
      # generate links to remotes
      link+="/${response[0]}"
    else
      # generate links to commands for a specific remote
      link+="/${PATH_INFO#/}?${response[1]}"
    fi

    echo '<li><a href="'${link}'">'${link##*/}'</a></li>'

    let data_n--
    continue
  fi

  case "${response[@]}" in
    "BEGIN"|"SUCCESS"|"LIST"*)
      : do noting
      ;;
    "SEND_ONCE "*)
      relistcmds=1
      ;;
    "DATA")
      wait_for_n=1
      continue
      ;;
    [0-9]*)
      if [ $wait_for_n -eq 1 ]
      then
        data_n="${response[0]}"
        wait_for_n=0
      fi
      ;;
    "END")
      if [ $relistcmds -eq 1 ]
      then
        relistcmds=0
        echo "LIST ${PATH_INFO#/}" >&10
      else
        break
      fi
      ;;
    "ERROR"|*)
      break
      ;;
  esac
  wait_for_n=0

done

exec 10>&-

echo '<hr/><li><a href="'${SCRIPT_NAME}'">START</a></li>'

cat <<'EOF'
</body>
</html>
EOF

